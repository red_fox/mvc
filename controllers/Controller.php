<?php
/**
 * Created by PhpStorm.
 * User: red_fox
 * Date: 09.01.19
 * Time: 1:49
 */
namespace controllers;

use models\Tasks;

class Controller
{
    /*
     * Список задач
     */
    public function tasks($params)
    {
        $tasksList = new Tasks();

        // Выбираем записи, устанавливаем параметры пагинации и сортировки
        $allTasks = $tasksList->getAll([
            'sort' => $params[1]?:'id',
            'limit' => 3,
            'offset' => $params[0]*3?:0,
        ]);

        $paginate = [];
        for ($i = 1; $i <= $allTasks[1]; $i++) {
            $paginate[] = [
                'page' => $i-1,
                'num' => $i,
            ];
        }

        $result = [
            'index' => 'Главная',
            'listItem' => $allTasks[0],
            'offset' => $params[0]?:0,
            'paginate' => $paginate,
            'sort' => $params[1]?:'id',
        ];

        return $result;
    }

    /*
     * Создать/изменить задачу
     */
    public function edit($params)
    {
        $tasksList = new Tasks();

        $result = [
            'id' => '',
            'name' => '',
            'email' => '',
            'text' => '',
            'status' => '',
        ];

        $result['edit'] = 'Создать задачу';

        // Если редактируем
        if (!empty($params)&&$_SESSION['admin']) {
            $result = $tasksList->getOne($params[0]);
            // Если не нашли запись
            if (!$result) {
                header('location: /notFound');
                exit;
            }
            $result['edit'] = 'Редактирование задачи';

            if ($_POST) {
                if ($tasksList->save($params[0], $_POST)) {
                    header('location: /');
                    exit;
                }
            }
        } else {

            if ($_POST) {
                if ($tasksList->save(false, $_POST)) {
                    header('location: /');
                    exit;
                }
            }
        }

        return $result;
    }

    /*
     * Авторизация
     */
    public function login($params)
    {
        // Если пришёл POST запрос
        if ($_POST) {
            if (($_POST['login'] == 'admin')&&($_POST['password'] == '123')) {

                $_SESSION['admin'] = true;

                // Редирект на главную
                header('location: /');
                exit;
            }
        }

        $result['login'] = 'Вход';

        return $result;
    }

    /*
     * Выход пользователя
     */
    public function logout($params)
    {
        $_SESSION['admin'] = false;

        // Редирект на главную
        header('location: /');
        exit;
    }

    /*
     * Страница не найдена
     */
    public function notFound()
    {
        $result = [
            'notFound' => 'Не найдено',
        ];

        return $result;
    }

}
