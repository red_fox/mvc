<?php
/**
 * Created by PhpStorm.
 * User: red_fox
 * Date: 09.01.19
 * Time: 1:44
 */

$mysqli = new mysqli('localhost', 'root', '', 'mvc');

/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$mysqli->set_charset("utf8");

// Запсукаем сессию
session_start();

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require $fileName;
}

spl_autoload_register('autoload');

use libraries\Router;
use libraries\View;
use controllers\Controller;

$router = new Router();

// Получаем параметры запроса
$route = $router->getRoute();

$controller = new Controller();

// Проверяем, есть ли такой экшн в контроллере
if (!method_exists($controller, $route['action'])) {
    $route['action'] = 'notFound';
}

$content = $controller->{$route['action']}($route['params']);

$content['isAdmin'] = $_SESSION['admin']?'':'none';
$content['isUser'] = $_SESSION['admin']?'none':'';

$view = new View();

$view->render($route['action'], $content);
