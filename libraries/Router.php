<?php
/**
 * Created by PhpStorm.
 * User: red_fox
 * Date: 09.01.19
 * Time: 2:03
 */

namespace libraries;

class Router
{
    private $route = '';

    public function __construct()
    {
        $this->route = $_GET['route'];
    }

    public function getRoute()
    {
        // Если параметра нет - направляем на главную страницу
        if (empty($this->route)) {
            $route[0] = 'tasks';
        } else {
            $route = explode('/', $this->route);
        }

        $result = [];
        $result['action'] = $route[0];
        unset($route[0]);
        $result['params'] = array_values($route);

        return $result;
    }
}
