<?php
/**
 * Created by PhpStorm.
 * User: red_fox
 * Date: 09.01.19
 * Time: 3:41
 */

namespace libraries;

class View
{
    // Метод сборки вьюшки
    public function render($action, $content)
    {
        $viewPath = 'views/';
        $viewFile = $viewPath . $action . '.html';

        // Если файла вьюшки не нашли
        if (!file_exists($viewFile)) {
            $viewFile = $viewPath . 'notFound.html';
        }

        // Собираем вьюшку
        $viewResult = $this->compose($viewFile, $content);

        $template = $viewPath . 'main.html';

        // Собираем шаблон
        $templateResult = $this->compose(
            $template,
            [
                'view' => $viewResult,
                'isAdmin' => $content['isAdmin'],
                'isUser' => $content['isUser']]
        );

        echo $templateResult;
    }

    // Метод поиска и замены данных
    public function compose($path, $content) {

        $result = file_get_contents($path);

        foreach ($content as $key=>$data) {
            // Если вдруг нашли дочерний подключаемый шаблон
            if (strstr($result, '{:' . $key . ':}')) {
                // Если имеем список элементов который нужно отобразить
                if (is_array($data)) {
                    $subData = '';
                    foreach ($data as $item) {
                        $subData .= $this->compose('views/' . $key . '.html', $item);
                    }
                    $data = $subData;
                } else {
                    $data = $this->compose('views/' . $key . '.html', $data);
                }
                $key = ':' . $key . ':';
            }
            $result = str_replace('{' . $key . '}', $data, $result);
        }

        return $result;
    }
}
