<?php
/**
 * Created by PhpStorm.
 * User: red_fox
 * Date: 09.01.19
 * Time: 4:59
 */

namespace models;

class Tasks
{
    protected $db;

    public function __construct()
    {
        global $mysqli;
        $this->db =& $mysqli;
    }

    /*
     * Метод получения записи
     */
    public function getOne($id)
    {
        $result = $this->db->query('SELECT * FROM tasks WHERE id = ' . $id);

        if ($row = $result->fetch_array()) {
            $result->close();

            switch ($row['status']) {
                case 0: $row['status'] = ''; break;
                case 1: $row['status'] = 'checked'; break;
            }

            return $row;
        }

        return false;
    }

    /*
     * Метод получения данных пакетом с параметрами
     */
    public function getAll($params = [])
    {
        $result = $this->db->query('SELECT * FROM tasks ORDER BY ' . $params['sort']);

        if ($result) {
            $resultArray = [];

            while ($row = $result->fetch_array()) {
                switch ($row['status']) {
                    case 0: $row['status'] = 'Не выполнено'; break;
                    case 1: $row['status'] = 'Выполнено'; break;
                }
                $resultArray[] = $row;
            }

            $result->close();

            $tasks = array_slice($resultArray, $params['offset'], $params['limit']);

            // Получаем количество страниц пагинатора
            $totalPages = ceil(count($resultArray)/$params['limit']);

            return [$tasks, $totalPages];
        }

        return false;
    }

    /*
     * Сохранение записи
     */
    public function save($id = false, $params)
    {
        if ($id) {
            $status = !$params['status']?null:1;

            $result = $this->db->query('UPDATE tasks SET name = \'' . addslashes($params['name']) . '\', email = \'' . addslashes($params['email']) . '\', text = \'' . addslashes($params['text']) . '\', status = \'' . $status . '\' WHERE id = ' . $id);
        } else {
            $result = $this->db->query('INSERT INTO tasks (id, name, email, text)' .
                'VALUES (NULL, \'' . addslashes($params['name']) . '\', \'' . addslashes($params['email']) . '\', \'' . addslashes($params['text']) . '\')');
        }

        if (!$result) {
            echo ($this->db->error);die;
            return false;
        }

        return true;
    }
}
